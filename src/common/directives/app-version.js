angular.module('directive.app-version', ['constants'])
    .directive('appVersion', function (package) {
        return function (scope, elm, attrs) {
            elm.text(package.version);
        };
    })
;
