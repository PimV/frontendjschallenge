angular.module('directive.app-title', ['constants'])
    .directive('appTitle', function (package) {
        return function (scope, elm, attrs) {
            var title = package.title;
            if(title !== undefined && title !== null) {
                elm.text(title);
            } else {
                elm.text('Unknown app title');
            }
        };
    })
;
