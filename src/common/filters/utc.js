angular.module('filter-utc', [])
    .filter('utc', function () {
        return function (input, pattern) {
            var result = moment.utc(input);
            return result.format(pattern);
        };
    });
