angular.module('lumichat', [
    'templates-app',
    'templates-common',
    'ngRoute',
    'ngCookies',
    'ngSanitize',
    'constants',
    'users',
    'chat',
    'directive.app-title',
    'directive.app-version',
    'filter-utc',
    'ui.keypress',
    'ngAnimate'
])

.config(function ($locationProvider, $routeProvider) {
    $locationProvider.html5Mode(false);
    $routeProvider.otherwise({redirectTo:'/user'});
})

.constant('BASE_URL', 'http://localhost:8080/server')

.run(function($rootScope, $location, $log, $cookieStore, User, UserService) {

    $rootScope.user = null;

    $rootScope.isRegistered = function() {
        $log.info('isRegistered on root');
        return UserService.isRegistered();
    };

    $rootScope.signOut = function() {
        $log.info('signOut');
        $cookieStore.remove('registeredUser');
        $rootScope.user = null;
        $location.path('/user');
    };

    $rootScope.$on('userRegistered', function(event, newUser) {
        $log.info('userRegistered');
        $cookieStore.put('registeredUser', newUser);
        $rootScope.user = newUser;
        $location.path('/chat');
    });

    $rootScope.$on('$routeChangeStart', function(event, next, current) {
        if ( $rootScope.user == null ) {
            var isRegistered = $rootScope.isRegistered();
            if (!isRegistered) {
                $location.path('/user');
            }
        }
    });

    $rootScope.notification = function(title, text) {
        $.gritter.add({
            title: title,
            text: text,
            time: 3000
        });
    };
})

;