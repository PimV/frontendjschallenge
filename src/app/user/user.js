angular.module('users', ['ngResource'])

    .config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/user', {
        templateUrl:'user/user.tpl.html',
        controller:'UserCtrl'
    });
}])

    .factory('User', function ($resource, BASE_URL) {
        return $resource(BASE_URL);
    })

    .factory('UserService', function ($log, User, $cookieStore, $rootScope) {
        return {
            isRegistered:function() {
                $log.info('isRegistered');
                var registeredUser = $cookieStore.get('registeredUser');
                if(!_.isUndefined(registeredUser) && _.isNull($rootScope.user)) {
                    $rootScope.user = new User({name:registeredUser.name});
                }
                return !_.isNull($rootScope.user);
            },


            findAllUsers:function (callback) {
                return User.query(callback);
            },

            registerUser:function (username, success, error) {
                return User.save({name:username}, success, error);
            }
        };
    })

    .controller('UserCtrl', function ($scope, $log, $sanitize, UserService) {

        $scope.newUsername = undefined;
        $scope.errorMessage = undefined;

        $scope.registerUser = function () {
            $scope.newUsername = $sanitize($scope.newUsername);
            $log.info('Trying to register user: ' + $scope.newUsername);
            if(!_.isUndefined($scope.newUsername) && !_.isNull($scope.newUsername)) {
                UserService.registerUser($scope.newUsername, $scope.registrationSuccess, $scope.registrationFailed);
            }
        };

        $scope.registrationSuccess = function (newUser) {
            $log.info('Registration succeeded for \'' + $scope.newUsername + '\'');
            $scope.$emit('userRegistered', newUser);
        };

        $scope.registrationFailed = function (error) {
            $log.error('Failed registering \'' + $scope.newUsername + '\'');
            if(error.status == 409) {
                $scope.errorMessage = 'There already is a \'' + $scope.newUsername + '\' chatting!';
            } else if(!_.isUndefined(error.message)) {
                $scope.errorMessage = error.message;
            } else {
                $scope.errorMessage = 'Something bad happened :(';
            }
        };
    })
;