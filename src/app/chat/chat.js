angular.module('chat', ['ngResource'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/chat', {
            templateUrl:'chat/chat.tpl.html',
            controller:'ChatCtrl'
        });
    }])

    .factory('ChatService', function ($log, $http, BASE_URL, User) {
        return {
            retrieveMessageForUSer:function (user, callback) {
                $http.get(BASE_URL + '/' + user.name).success(callback);
            },

            sendMessage:function(message, recipient, user, callback) {
                $log.info('sendMessage');

                $http.post(BASE_URL + '/' + recipient, {sender:user.name, content:message}).success(callback);
            }
        };
    })

    .controller('ChatCtrl', function ($scope, $log, $location, $timeout, ChatService, UserService) {

        $scope.chatters = [];
        $scope.messages = [];
        $scope.action = 'read';
        $scope.recipient = null;

        $scope.init = function() {
            $scope.retrieveChatters();
            $scope.refreshMessages();
        };

        $scope.retrieveChatters = function () {
            $log.info('retrieveChatters');
            $scope.chatters = [];
            UserService.findAllUsers(function (users) {
                angular.forEach(users, function(user) {
                    if($scope.user.name !== user.name) {
                        $scope.chatters.push(user);
                    }
                });
            });
        };

        $scope.refreshMessages = function() {
            $log.info('refreshMessages');
            ChatService.retrieveMessageForUSer($scope.user, function(messages) {
                if(messages.length === 0) {
                    return false;
                }

                    var count = 0;
                    angular.forEach(messages, function(message) {
                        var found = false;
                        angular.forEach($scope.messages, function(readMessage) {
                            if(message.id == readMessage.id) {
                                found = true;
                                return false;
                            }
                        });
                        if(!found) {
                            count++;
                            $scope.messages.push(message);
                            $scope.notification('Message recieved', 'You have a message from ' + message.sender);
                        }

                    });
                    if(count > 0) {
                        $log.info('Got ' + count + ' new messages');
                    }
            });
        };

        $scope.sendMessage = function (messageToSend) {
            ChatService.sendMessage(messageToSend, $scope.recipient, $scope.user, function() {
                $log.info('Message sent');
                // on succes clear recipient
                $scope.cancelMessage();
            });
        };

        $scope.reply = function(message) {
            $scope.selectRecipient(message.sender);
        };

        $scope.selectRecipient = function(recipient) {
            $log.info('selecetRecipient');
            $scope.recipient = recipient;
            $scope.action = 'send';
        };

        $scope.cancelMessage = function() {
            $log.info('cancelMessage');
            $scope.recipient = null;
            $scope.action = 'read';
        };

        var pollChatters = function() {
            $timeout(function() {
                if($scope.user != null) {
                    $scope.retrieveChatters();
                    pollChatters();
                }
            }, 10000);
        };
        pollChatters();

        var pollMessages = function() {
            $timeout(function() {
                if($scope.user != null) {
                    $scope.refreshMessages();
                    pollMessages();
                }
            }, 5000);
        };
        pollMessages();
    }

);