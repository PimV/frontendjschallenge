# Chatserver Rest API

## Create a new user

> POST /server

## Retrieve all users

> GET /server

## Send a message to a user

> POST /server/{recipient}

## Retrieve all messages sent to a user

> GET /server/{recipient}

## Retrieve a single message sent to a user

> GET /server/{recipient}/{id}

Not implemeted!
