LumiChat, a Luminis JS Challenge Frontend 2013
==============================================

##### Pim Voeten

# Description

LumiChat is an HTML5/Javascript chatbox webapplication for the Luminis javascript challenge.
The primary goal was to make a `working chatbox` with all the features needed to do some chatting.
The secondary goal was to implement a `good development stack`.

I started working on an AngularJS application from scratch, trying to use a directory structure advised in the awesome book
[Mastering web application development with AngularJS](http://www.amazon.com/Mastering-Web-Application-Development-AngularJS/dp/1782161821/ref=pd_cp_b_0).
Basic features were implemented one by one and some nice extra's were added like a simple animation and growly notifications.
So, it took me a little less than a week to get the chatbox working, but I was not yet satisfied. in Java we try to automate as much as possible. My second goal was to setup a good development
stack and my webapplication was still lacking that.

After some googling I was brought to [ng-boilerplate](https://github.com/ngbp/ng-boilerplate), a very nice project kickstart initiative.
`ng-boilerplate` gives you a directory structure, an automated task runner (`Grunt`), a dependency manager (`Bower`), an automated test framework (`Karma`),
building and packaging methods and other nice features.

Both Grunt and Bower were unfamiliar to me. Ng-boilerplate gave me a good headstart to these tools, but more time is needed to
get to know them well.

# Setup

I assume you already have Node.js installed on your system.

To run LumiChat, this is what you must do:

```sh
$ git clone git@bitbucket.org:PimV/frontendjschallenge.git
$ cd frontendjschallenge
$ npm install
$ bower install
```

## Configuration

If you want to use a chat server not running on localhost:

```sh
Open src/app/lumichat.js
Change BASE_URL to match the right chatserver
```

# Frameworks/libs/tools used:

- AngularJS 1.2.0-rc3
- Bootstrap 2.3.2
- MomentJS 2.2.1
- Lo-dash 2.2.1
- jquery 1.8.2
- ui-utils 0.0.4
- Gritter 1.8.0
- ng-boilerplate 0.3.1
- Grunt
- Bower
- BitBucket (git)
- Node
- IntelliJ

## Grunt.js

This text is borrowed from `ng-boilerplate`...

[Grunt](http://gruntjs.com) is a JavaScript task runner that runs on top of
Node.js. Most importantly, Grunt brings us automation. There are lots of steps
that go into taking our manageable codebase and making it into a
production-ready website; we must gather, lint, test, annotate, and copy files
about. Instead of doing all of that manually, we write (and use others') Grunt
tasks to do things for us.

When we want to build our site, we can just type:

```sh
$ grunt
```

This will do everything needed and place our built code inside a folder called
`bin/`. Even more magical, we can tell Grunt to watch for file changes we make
so it can re-build our site on-the-fly:

```sh
$ grunt watch
```

The built files will be in `build/`.

The next time we change a source file, Grunt will re-build the changed parts of
the site. If you have a Live Reload plugin installed in your browser, it will
even auto-refresh your browser for you. You lazy bum.

Grunt is controlled through `Gruntfile.js`. This file is heavily documented in
the source, so I will only provide a high-altitude overview here. Also note that
unless you need to modify the build process, you don't need to know anything
else from this section. The two commands above really are all you need to know
to get started with `ngBoilerplate`. But for those curious or looking to go a
little more advanced, here's what you'll find.

First, we tell Grunt which tasks we might want to use:

```js
// ...
grunt.loadNpmTasks('grunt-recess');
grunt.loadNpmTasks('grunt-contrib-clean');
grunt.loadNpmTasks('grunt-contrib-copy');
grunt.loadNpmTasks('grunt-contrib-jshint');
// ...
```

Each of these tasks must already be installed. Remember the dependencies from
`package.json` that NPM installed for us? Well, this is where they get used!

Then we get the opportunity to tell the tasks to behave like we want by
defining a configuration object. While we can (and do) define all sorts of
custom configuration values that we reference later on, tasks look for
configuration properties of their own name. For example, the `clean` task just
takes an array of files to delete when the task runs:

```js
clean: [ '<%= build_dir %>', '<%= compile_dir %>' ],
```

In Grunt, the `<%= varName %>` is a way of re-using configuration variables.
In the `build.config.js`, we defined what `build_dir` meant:

```js
build_dir: 'build',
```

When the clean task runs, it will delete the `build/` folder entirely so that
when our new build runs, we don't encounter any problems with stale or old
files. Most tasks, however, have considerably more complicated configuration
requirements, but I've tried to document what each one is doing and what the
configuration properties mean. If I was vague or ambiguous or just plain
unclear, please file an issue and I'll get it fixed. Boom - problem solved.

After our configuration is complete, we can define some of our own tasks. For
example, we could do the build by running all of the separate tasks that we
installed from NPM and configured as above:

```sh
$ grunt clean
$ grunt html2js
$ grunt jshint
$ grunt karma:continuous
$ grunt concat
$ grunt ngmin:dist
$ grunt uglify
$ grunt recess
$ grunt index
$ grunt copy
```

But how automated is that? So instead we define a composite task that executes
all that for us. The commands above make up the `default` tasks, which can be
run by typing *either* of these commands:

```js
$ grunt
$ grunt default
```

We also define the `watch` task discussed earlier. This is covered in more
detail in the main (README)[README.md].

Grunt is the engine behind `ngBoilerplate`. It's the magic that makes it move.
Just getting started, you won't need to alter `Gruntfile.js` at all, but
as you get into more advanced application development, you will probably need to
add more tasks and change some steps around to make this build your own.
Hopefully, this readme and the documentation within `Gruntfile.js` (as well as
of course the documentation at gruntjs.com) will set you on the right path.

## Bower

[Bower](bower.io) is a package manager for the web. It's similar in many
respects to NPM, though it is significantly simpler and only contains code for
web projects, like Twitter Bootstrap and its AngularJS counterpart Angular
Bootstrap. Bower allows us to say that our app depends in some way on these
other libraries so that we can manage all of them in one simple place.

`ngBoilerplate` comes with a `bower.json` file that looks something like this:

```js
{
  "name": "LumiChat-App",
  "version": "0.0.1",
  "devDependencies": {
    "angular": "~1.2.0-rc.3",
    "angular-mocks": "~1.2.0-rc.3",
    "bootstrap": "~2.3.2",
    "angular-animate": "~1.2.0-rc.3",
    "angular-bootstrap": "~0.3.0",
    "angular-ui-router": "~0.0.1",
    "angular-ui-utils": "~0.0.4",
    "angular-route": "~1.2.0-rc.3",
    "angular-cookies": "~1.2.0-rc.3",
    "angular-sanitize": "~1.2.0-rc.3",
    "angular-resource": "~1.2.0-rc.3",
    "lodash": "~2.2.1",
    "momentjs": "~2.4.0",
    "gritter": "~1.8.0"
  },
  "dependencies": {}
}
```

This file is fairly self-explanatory; it gives the package name and version
(duplicated from `package.json`, but this is unavoidable) as well as a list of
dependencies our application needs in order to work. If we simply call

```sh
$ bower install
```

it will read these three dependencies and install them into the `vendor/` folder
(along with any dependencies they have) so that we can use them in our app. If
we want to add a new package like AngularUI's
[ngGrid](http://angular-ui.github.io/ng-grid/), then we can tell Bower to
install that from the web, place it into the `vendor/` folder for us to use, and
then add it as a dependency to `bower.json`:

```js
$ bower install angular-grid --save-dev
```

Bower can also update all of our packages for us at a later date, though that
and its many other awesome features are beyond the scope of this simple
overview.

One last thing to note is that packages installed with Bower are not
standardized, so we cannot automatically add them to the build process; anything
installed with Bower (or placed in the `vendor/` directory manually) *must* be
added to your `build.config.js` file manually; look for the Bower libs included
in `ngBoilerplate` by default in there to see what I mean.

# Issues

- No tests were written.
- `grunt compile` artifact is not working when deployed in Jetty.
- `grunt watch` not always working as I expect.
- Vendor assets and app assets get mixed up if not careful.
- Grunt is new to me.
- Bower is new to me.
- Less/css is new to me.
- Everything gets minified, uglified and concatenated. Getting to know to work with these new tools and build steps takes some time.